##Imagem base php8.1 apache

    - docker buildx build --platform linux/amd64,linux/arm64 -t jamirperoba/image-base-php8:latest --push .
    - docker buildx build --platform linux/amd64,linux/arm64 -t jamirperoba/image-base-php8:ubuntu-22 --no-cache --push .
    - docker buildx build --platform linux/amd64,linux/arm64 -t jamirperoba/image-base-php8:ubuntu-22-kafka-dev  --no-cache --push .
    - docker buildx build --platform linux/amd64,linux/arm64 -t jamirperoba/image-base-php8:ubuntu-23  --no-cache --push .
    - docker push jamirperoba/image-base-php8:latest
    - docker buildx build --platform linux/amd64,linux/arm64 -t jamirperoba/image-base-php8.2:v1 --no-cache --push .
    - docker buildx build --platform linux/amd64,linux/arm64 -t jamirperoba/image-base-php8.3 --no-cache --push .



Exemplo de docker-compose:
services:
    laravel-app:
            build:
            context: '.'
        container_name: laravel-app
        ports:
            - 8000:80
        volumes:
            - .:/var/www/html
        networks:
            backend:
                aliases:
                    - app
